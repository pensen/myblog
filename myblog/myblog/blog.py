from django.shortcuts import render,redirect,HttpResponseRedirect
from markdown import markdown
import os
import requests
import json



def index(request):
    #r = requests.get('https://img.xjh.me/random_img.php?type=bg&ctype=nature&return=json').json()
    #background = r['img']

    mulu = os.listdir('/root/myblog/myblog/static/text')

    re_file=[]
    for i in mulu:
        files = os.listdir('/root/myblog/myblog/static/text/'+i)

        for file in files:
            re_file.append("<a href=/"+i+'/?text='+file.replace('.md','')+">"+file.replace('.md','')+'</a>')




    return render(request, 'blog.html',{'re_file':re_file})


def text(request,year):

    r = requests.get('https://api.ixiaowai.cn/api/api.php?return=json')
    background = json.loads(r.text.encode('utf-8'))['imgurl']


    mulu = os.listdir('/root/myblog/myblog/static/text/')

    re_files = {}
    for j in mulu:
        files = os.listdir('/root/myblog/myblog/static/text/'+j)
        for i in files:
            re_files[i.replace('.md','')]=j




    if(year in mulu):
        if('text' in request.GET and request.GET['text'] and request.GET['text'] in re_files):
            print(request.GET["text"]+'.md')
            with open(f'/root/myblog/myblog/static/text/{re_files[request.GET["text"]]}/{request.GET["text"]}'+'.md') as f:
                file = f.read()



                html = markdown(file,extensions=[
                # 包含 縮寫、表格等常用擴展
                'markdown.extensions.extra',
                # 語法高亮擴展
                'markdown.extensions.codehilite',
                #允許我們自動生成目錄
                'markdown.extensions.toc',
                ])

                return render(request,'text.html',{'text':html,'background':background})
        else:
            re_file =[]
            for i in re_files:
                if re_files[i] == year:
                    re_file.append('<a href="/'+year+'?text='+i +'">'+ i +'</a>')


            return render(request,'fenlei.html',{'re_file':re_file,'background':background})


    else:
        return redirect('/')



def fenlei(request):
    r = requests.get('https://api.ixiaowai.cn/api/api.php?return=json')
    background = json.loads(r.text.encode('utf-8'))['imgurl']

    mulu = os.listdir('/root/myblog/myblog/static/text/')

    re_file =[]

    for i in mulu:
        re_file.append('<a href="/' + i + '">'+ i +'</a>')


    return render(request,'fenlei.html',{'background':background,'re_file':re_file})

