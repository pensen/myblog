window.addEventListener('load', function() {

    var nav = document.querySelector('#nav');
    var music = document.querySelector('#music');
    var title = document.querySelector('.title');
    var body = document.querySelector('body');
    var text = document.querySelector('.text');
    //var waifu = document.querySelector('#waifu');
    var gundong_2 = document.querySelector('.gundong_2');
    var beian = document.querySelector('.beian');

    var w_width = window.innerWidth;
    if (w_width < 800 ) {
        body.style.width = w_width + 'px';
        nav.style.width = w_width +'px';
        moblie(w_width);
    }

    window.onresize = function() {
        var w_width = window.innerWidth;
        if (w_width < 750) {
            console.log(w_width);
            body.style.width = w_width + 'px';
            nav.style.width = w_width + 'px';
            moblie(w_width);
        } else {
            nav.className = 'nav';
            nav.style.width = '200px';
            music.style.display = 'block';
            title.className = 'title';
            text.className ='text';
            //waifu.style.left = '1250px';
            gundong_2.className ='gundong_2';
            beian.className ='beian';
        }

    }

    function moblie(w_width) {
        music.style.display = 'none';
        nav.className = 'mnav';
        title.className = 'mtitle';
        text.className ='mtext';
        gundong_2.className ='mgundong_2';
        beian.className ='mbeian';
    }
})
