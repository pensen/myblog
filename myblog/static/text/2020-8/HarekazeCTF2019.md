# encode_and_encode
```php
<?php
error_reporting(0);

if (isset($_GET['source'])) {
  show_source(__FILE__);
  exit();
}

function is_valid($str) {
  $banword = [
    // no path traversal
    '\.\.',
    // no stream wrapper
    '(php|file|glob|data|tp|zip|zlib|phar):',
    // no data exfiltration
    'flag'
  ];
  $regexp = '/' . implode('|', $banword) . '/i';
  if (preg_match($regexp, $str)) {
    return false;
  }
  return true;
}

$body = file_get_contents('php://input');
$json = json_decode($body, true);

if (is_valid($body) && isset($json) && isset($json['page'])) {
  $page = $json['page'];
  $content = file_get_contents($page);
  if (!$content || !is_valid($content)) {
    $content = "<p>not found</p>\n";
  }
} else {
  $content = '<p>invalid request</p>';
}

// no data exfiltration!!!
$content = preg_replace('/HarekazeCTF\{.+\}/i', 'HarekazeCTF{&lt;censored&gt;}', $content);
echo json_encode(['content' => $content]);
```
先分析源码
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200728153252415.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
这一段是设置了一段正则匹配，过滤一些协议和flag

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200728153407794.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)
这一大段是通过php://input协议
接受我们的POST传的内容
然后将内容进行json解码

接着就是条件判断
判断post传的内容是否为数组 以及是否存在json，json里是否有page索引

然后就是读取page的文件内容



最后进行替换
将HarekazeCTF{}，括号里的内容替换为<conserd>

这题的思路不难猜，就是需要绕过那段正则来读取flag

通过搜索发现json_decode会自动解析unicode编码
因此我们可以用unicode编码来绕过上面的正则匹配
[unicode编码网站](https://www.branah.com/unicode-converter)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200728161058759.png)
由于这个是 比赛的题目，所以这个字符替换会把flag给弄没了，此时可以用协议来直接读取
php伪协议即可
unicode编码传入即可
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200728161209709.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NvcFJvbWVv,size_16,color_FFFFFF,t_70)





